# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Helper Methods
def generate_random_string(length)
  ('a'..'z').to_a.shuffle.take(length).join
end

def generate_random_email
  "#{generate_random_string(5)}@#{generate_random_string(4)}.com"
end

def create_scratchers!(number)
  puts "Creating #{number} Scratchers"
  number.times.each do
    Scratcher.create!(name: generate_random_string(6), email: generate_random_email)
  end
  puts "Scratchers successfully created!"
end

def create_users!
  puts "Creating 4 Users"

  User.create!(name: 'Pepe', email: 'pepe@example.com')
  User.create!(name: 'Luigi', email: 'luigi@example.com')
  User.create!(name: 'Oscar', email: 'oscar@example.com')
  User.create!(name: 'Gomez', email: 'gomez@example.com')

  puts "Users successfully created!"
end

# Seeds
create_scratchers!(50)
create_users!
