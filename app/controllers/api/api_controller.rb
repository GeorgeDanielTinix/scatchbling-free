module Api
  class ApiController < ApplicationController
    before_filter :authenticate


     def authenticate
       api_key = params[:api_key]

       if api_key.blank?
         render json: { error: 'API key must be provided', status: 401 }, status: 401
       else
         unless valid_api_key?(api_key)
           render json: { error: 'Authentication failed', status: 401 }, status: 401
         end
       end
     end

    private

    def valid_api_key?(key)
      User.exists?(api_key: key)
    end
  end
end
