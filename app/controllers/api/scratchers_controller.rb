class Api::ScratchersController < Api::ApiController
  before_action :set_scratcher, only: [:show, :edit, :update]

  def index
    render json: {scratchers: Scratcher.select(:id, :name, :email), status: 200 }
  end

  def show
    render json: {scratcher: @scratcher, status: 200}
  end

  ##
  # In next two methods I was checking by myself the needed params presence
  # because that is the regular way in which the api works.
  # Then I changed my mind and I prefered let ActiveRecord validations handle
  # this for me which will result in a cleaner implementation, the only side effect
  # is rails raw error messages are not very user-friendly.
  ##
  def edit
    @scratcher.name = params[:name]
    @scratcher.email = params[:email]

    if @scratcher.save
      render json: { scratcher: @scratcher, status: 200 }
    else
      render json: { error: @scratcher.errors.messages, status: 200 }
    end
  end

  def create
    scratcher = Scratcher.new(scratcher_params)

    if scratcher.save
      render json: { scratcher: scratcher, status: 200 }
    else
      render json: { error: scratcher.errors.messages, status: 200 }
    end
  end

  private

  def set_scratcher
    id = params[:id]

    if id.present?
      if Scratcher.exists?(id)
        @scratcher = Scratcher.find(params[:id])
      else
        render json: { error: "Scratcher with id: #{id} could not be found", status: 200 }
      end
    else
      render json: { error: 'ID must be provided', status: 401 }
    end
  end

  def scratcher_params
    params.require(:scratcher).permit(:name, :email)
  end
end
